package ru.androiddevschool.clicktofly.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import ru.androiddevschool.clicktofly.ClickToFly;

public class DesktopLauncher {
	public static void main (String[] arg) {
		System.setProperty("user.name", "EnglishWords");
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 360;
		config.height = 640;
		new LwjglApplication(ClickToFly.getInstance(), config);
	}
}
