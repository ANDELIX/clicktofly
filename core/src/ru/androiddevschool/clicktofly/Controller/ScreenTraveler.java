package ru.androiddevschool.clicktofly.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import ru.androiddevschool.clicktofly.ClickToFly;

/**
 * Created by 05k1102 on 01.04.2017.
 */
public class ScreenTraveler extends ClickListener {
    private String name;
    public ScreenTraveler(String name){
        this.name = name;
    }
    public void clicked(InputEvent event, float x, float y) {
        ClickToFly.getInstance().setScreen(name);
    }
}
