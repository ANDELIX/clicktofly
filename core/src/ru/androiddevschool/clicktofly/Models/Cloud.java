package ru.androiddevschool.clicktofly.Models;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.Random;

import ru.androiddevschool.clicktofly.Utils.Values;

/**
 * Created by 05k1102 on 22.04.2017.
 */
public class Cloud extends Image {
    private static Random r = new Random();
    private float v;
    private int d;

    public Cloud(TextureRegionDrawable img, float velocity, int direction, float y) {
        super(img);
        this.v = velocity;
        this.d = direction;
        setY(y + 200 + r.nextInt(5000));
        if (d>0){
            setX(-getWidth());
        }else{
            setX(Values.WORLD_WIDTH);
        }
    }

    public void act(float delta) {
        moveBy(d * v * delta, 0);
    }
}