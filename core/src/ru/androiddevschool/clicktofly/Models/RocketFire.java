package ru.androiddevschool.clicktofly.Models;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import ru.androiddevschool.clicktofly.Utils.Values;

/**
 * Created by 05k1102 on 13.05.2017.
 */
public class RocketFire extends Image {
    private TextureRegionDrawable frame;
    private TextureRegionDrawable[] frames;
    private float time;
    private float frameDuration;

    public RocketFire(TextureRegionDrawable[] fire) {
        super(fire[0]);
        frame = fire[0];
        frames = fire;
        time = 0;
        frameDuration = Values.tapDuration/fire.length;
    }

    public void act(float delta) {
        super.act(delta);
        time += delta;
        time %= Values.tapDuration;
        frame = frames[((int) (time/frameDuration))];
        setDrawable(frame);
        setY(-frame.getRegion().getRegionHeight());
    }
}
