package ru.androiddevschool.clicktofly.Models;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import ru.androiddevschool.clicktofly.ClickToFly;
import ru.androiddevschool.clicktofly.Utils.Values;

/**
 * Created by 05k1102 on 13.05.2017.
 */
public class Player extends Group {
    private float v;
    private float time;
    private Image rocket;
    private RocketFire fire;
    private int tapCount;
    private boolean tapped;

    public Player(TextureRegionDrawable rocketimage, TextureRegionDrawable[] fireframes) {
        rocket = new Image(rocketimage);
        rocket.moveBy(-rocketimage.getRegion().getRegionWidth() / 2, 0);
        fire = new RocketFire(fireframes);
        tapCount = 0;
        time = 0;
        addActor(rocket);
        addActor(fire);
        fire.setVisible(false);
        setOrigin(rocket.getWidth() / 2, rocket.getHeight() / 2 );
        fire.setX(-fire.getWidth() / 2);
        fire.setY(-fire.getHeight() );
        tapped = false;
    }

    public void act(float delta) {
        super.act(delta);
        time += delta;
        if (time >= Values.tapDuration && tapCount > 0) {
            tapCount = 0;
            time %= Values.tapDuration;
            if (tapCount == 0 ) fire.setVisible(false);
        } else {
            v -= Values.g * delta;
            if (v < Values.minV) v = Values.minV;
        }

        moveBy(0, v * delta);
        if (getY() < 400) {
            setY(400);
            v = 0;
        }
        moveBy(0, v * delta);
        if (getY() <= 400 && v <= 0 && tapped) {
            ClickToFly.getInstance().setScreen("Lose");
        }

        if (v < 30 && getRotation() > -180) rotateBy(v / 50 * delta);
        if (tapCount > 0 && getRotation() < 0) rotateBy(v / 50 * delta);
        if ( v > 0 && getRotation() < 0) rotateBy(v / 50 * delta);
    }


    public void tap() {
        fire.setVisible(true);
        tapCount++;
        v += Values.dv;
        if (v > Values.maxV) v = Values.maxV;
        if (getY() > 500) tapped = true;
        if (tapCount==0) fire.setVisible(true);

    }
}
