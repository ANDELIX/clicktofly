package ru.androiddevschool.clicktofly.Models;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.sun.media.jfxmedia.events.PlayerTimeListener;

import ru.androiddevschool.clicktofly.Utils.Assets;
import ru.androiddevschool.clicktofly.Utils.Values;

/**
 * Created by 05k1102 on 08.04.2017.
 */
public class World extends Stage {
    private Player player;
    private float score;

    public World(Viewport viewport, Batch batch) {
        super(viewport, batch);

        Image ground = new Image(Assets.get().images.get("grass"));
        ground.setSize(1080, 300);
        ground.setPosition(0, 0);
        addActor(ground);

        Image startArea = new Image(Assets.get().images.get("startarea"));
        startArea.setSize(325, 110);
        startArea.setPosition(Values.WORLD_WIDTH / 2, 300, Align.bottom);
        addActor(startArea);



        TextureRegionDrawable[] fire = new TextureRegionDrawable[2];
        fire[0] = Assets.get().images.get("Fire00");
        fire[1] = Assets.get().images.get("Fire01");
        player = new Player(Assets.get().images.get("SpaceShip01"), fire);
        player.setPosition(Values.WORLD_WIDTH / 2, 0, Align.bottom);
        player.setOrigin(Align.center);
        addActor(player);
    }

    public boolean keyDown(int keycode) {
        player.tap();
        return false;
    }

    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        player.tap();
        return false;
    }


    public Player getPlayer() {
        return player;
    }

    public int getScore() {
        return ((int) player.getY()) / 10 + ((int) player.getY() / 100 * 10 - 80);
    }

    public void addScore(float score) {
        this.score += score;
    }

}
