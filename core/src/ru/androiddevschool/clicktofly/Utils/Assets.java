package ru.androiddevschool.clicktofly.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.HashMap;

/**
 * Created by 05k1102 on 01.04.2017.
 */
public class Assets {
    private static Assets ourInstance = new Assets();

    public static Assets get() {
        return ourInstance;
    }

    private Assets() {
        initImages();
        initFonts();
        //for(String name : images.keySet()) System.out.println(name);
        initLabelStyles();
        initStyles();
       // initAnimations();
    }

    private void initLabelStyles() {
        labelStyles = new HashMap<String, Label.LabelStyle>();
        labelStyles.put("menuSymbols", new Label.LabelStyle(fonts.get("menuSymbol2"), null));
    }

  /*  private void initAnimations() {
        animations = new HashMap<String, Animation<TextureRegionDrawable>>();
    }*/

    private void initFonts() {
        fonts = new HashMap<String, BitmapFont>();
        fonts.put("simple", new BitmapFont());

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("Fonts/spaceage.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя?!";
        parameter.color = new Color(0xe0e743FF);
        parameter.borderColor = new Color(0x898F07FF);
        parameter.size = 100;
        fonts.put("menu", generator.generateFont(parameter));

        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.characters = "?";
        parameter.color = new Color(0xe0e743FF);
        parameter.borderColor = new Color(0x898F07FF);
        parameter.size = 250;
        fonts.put("menuSymbol", generator.generateFont(parameter));
/*
        generator = new FreeTypeFontGenerator(Gdx.files.internal("Fonts/fontello.ttf"));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.characters = String.format("\uE800\uE801\uE802\uF204\uF205");
        parameter.color = new Color(0xe0e743FF);
        parameter.borderColor = new Color(0x898F07FF);
        parameter.size = 100;
        fonts.put("menuSymbol2", generator.generateFont(parameter));
*/
    }

    private void initImages() {
        images = new HashMap<String, TextureRegionDrawable>();
        addImagesFolder("/other");
        addImagesFolder("/Menu");
        addImagesFolder("/Play");
        addImagesFolder("/Pause");
        addImagesFolder("/World");
        addImagesFolder("/Fonts");
        addImagesFolder("/Models");
        addImagesFolder("/Store");
    }

    private void initStyles() {
        buttonStyles = new HashMap<String, Button.ButtonStyle>();
        buttonStyles.put("back",
                new Button.ButtonStyle(
                        images.get("BackBlue"),
                        images.get("BackBlue01"),
                        null
                )
        );
        buttonStyles.put("play",
                new Button.ButtonStyle(
                        images.get("Play"),
                        images.get("Play"),
                        null
                )
        );
        buttonStyles.put("menu",
                new Button.ButtonStyle(
                        images.get("Menu"),
                        images.get("Play"),
                        null
                )
        );
        buttonStyles.put("name",
                new Button.ButtonStyle(
                        images.get("Name"),
                        images.get("Name"),
                        null
                )
        );
        buttonStyles.put("pause",
                new Button.ButtonStyle(
                        images.get("Pause00"),
                        images.get("Pause01"),
                        null
                )
        );
        buttonStyles.put("store",
                new Button.ButtonStyle(
                        images.get("Store"),
                        images.get("store"),
                        null
                )
        );
        buttonStyles.put("shop",
                new Button.ButtonStyle(
                        images.get("korzina"),
                        images.get(""),
                        null
                )
        );
        buttonStyles.put("settings",
                new Button.ButtonStyle(
                        images.get("SettingsBlue"),
                        images.get(""),
                        null
                )
        );
        buttonStyles.put("skin00",
                new Button.ButtonStyle(
                        images.get("SpaceShip00"),
                        images.get("SpaceShip00"),
                        null
                )
        );

        buttonStyles.put("skin01",
                new Button.ButtonStyle(
                        images.get("SpaceShip01"),
                        images.get("SpaceShip01"),
                        null
                )
        );
        buttonStyles.put("skin02",
                new Button.ButtonStyle(
                        images.get("SpaceShip02"),
                        images.get("SpaceShip02"),
                        null
                )
        );
        buttonStyles.put("skin03",
                new Button.ButtonStyle(
                        images.get("Rocket01"),
                        images.get("Rocket01"),
                        null
                )
        );
/*
        buttonStyles.put("sound",
                new Button.ButtonStyle(
                        new Label("\uE800", labelStyles.get("menuSymbol")),
                        null,
                        new Label("\uE801", labelStyles.get("menuSymbol"))
                )
        );
        */
        buttonStyles.put("sound",
                new Button.ButtonStyle(
                        images.get("SoundOn"),
                        null,
                        images.get("SoundOff")
                )
        );

        buttonStyles.put("window",
                new Button.ButtonStyle(
                        images.get("StoreWindow00"),
                        null,
                        images.get("StoreWindows01")
                )
        );
    }

    public void addImagesFolder(String folderName) {
        FileHandle file = Gdx.files.local(folderName);
        for (FileHandle f : file.list()) {
            if (f.name().endsWith("png"))
                images.put(f.nameWithoutExtension(), getDrawable(f));
        }
    }

    public TextureRegionDrawable getDrawable(FileHandle file) {
        return new TextureRegionDrawable(new TextureRegion(new Texture(file)));
    }

    public HashMap<String, TextureRegionDrawable> images;
    public HashMap<String, Button.ButtonStyle> buttonStyles;
    public HashMap<String, BitmapFont> fonts;
    public HashMap<String, Label.LabelStyle> labelStyles;
   // public HashMap<String, Animation<TextureRegionDrawable>> animations;

}