package ru.androiddevschool.clicktofly.Utils;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by 05k1102 on 22.04.2017.
 */
public class Background extends Actor {
    private Texture src;

    public Background(Texture src) {
        super();
        this.src = src;
        src.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        setPosition(0, 0);
        setSize(1200,5000);
    }

    public void draw(Batch batch, float parentAlpha) {
        batch.draw(src, getX(), getY(), 0, 0, (int)getWidth(), (int)getHeight());
    }
}
