package ru.androiddevschool.clicktofly.Utils;

import com.badlogic.gdx.Gdx;

/**
 * Created by 05k1102 on 08.04.2017.
 */
public class Values {
    public static final float WORLD_WIDTH = 1080;
    public static final float WORLD_HEIGHT = 1920;
    public static final float ppuX = Gdx.graphics.getWidth() / WORLD_WIDTH;
    public static final float ppuY = Gdx.graphics.getHeight() / WORLD_HEIGHT;
    public static final float maxV = 600;
    public static final float minV = -maxV * 2;
    public static final float g = maxV / 2;
    public static final float dv = maxV / 5;
    public static final float tapDuration = 0.3f;
}
