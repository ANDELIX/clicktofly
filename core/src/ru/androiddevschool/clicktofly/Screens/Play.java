package ru.androiddevschool.clicktofly.Screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;


import java.util.Random;

import javafx.scene.layout.Background;
import ru.androiddevschool.clicktofly.Controller.ScreenTraveler;
import ru.androiddevschool.clicktofly.Models.Cloud;
import ru.androiddevschool.clicktofly.Models.Player;
import ru.androiddevschool.clicktofly.Models.World;
import ru.androiddevschool.clicktofly.Utils.Assets;
import ru.androiddevschool.clicktofly.Utils.Values;

/**
 * Created by 05k1102 on 01.04.2017.
 */
public class Play extends StdScreen {
    World world;
    private Player player;
    private float v;
    private Timer timer;
    private Label score;
    private int bgState;
    private Image sky;
    private Image sun;
    private Image gradient;
    private Image space;
    private static final Random r = new Random();

    public Play(SpriteBatch batch) {
        super(batch);
        init();
        timer = new Timer();


            timer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    Cloud cloud;
                    TextureRegionDrawable img;
                    img = Assets.get().images.get(String.format("Cloud%02d", r.nextInt(8)));
                    cloud = new Cloud(img, r.nextInt(40) + 20, r.nextBoolean() ? 1 : -1, player.getY());
                    cloud.setSize(200, 90);
                    if (world.getScore()<2000){
                    world.addActor(cloud);}
                }
            }, 0f, 2f);

    }

    private void init() {
        initBg();
        initWorld();
        initUi();
    }

    private void initBg() {
        bg.clear();
        bgState = 0;
        sky = new Image(Assets.get().images.get("Sky"));
        sky.setSize(1080, 1920);
        sky.setPosition(0, 0);
        bg.addActor(sky);

        gradient = new Image(Assets.get().images.get("Play/Gradient"));
        gradient.setSize(1080, 1920);
        gradient.setPosition(0, Values.WORLD_HEIGHT);

        space = new Image(Assets.get().images.get("space"));
        space.setSize(1080, 1920);
        space.setPosition(0, Values.WORLD_HEIGHT);

        sun = new Image(Assets.get().images.get("sun_shiny"));
        sun.setSize(300,300);
        sun.setPosition(50, 1500);


        bg.addActor(gradient);
        bg.addActor(space);
        bg.addActor(sun);

    }


    private void initWorld() {
        world = new World(stage.getViewport(), stage.getBatch());
        //world.clear();
        stage = world;
        player = world.getPlayer();
    }

    private void initUi() {
        ui.clear();
        Button button;
        button = new Button(Assets.get().buttonStyles.get("pause"));
        button.addListener(new ScreenTraveler("Pause"));
        button.setPosition(Values.WORLD_WIDTH - button.getWidth(), Values.WORLD_HEIGHT - button.getHeight());
        button.setSize(150, 150);
        ui.addActor(button);

        score = new Label("", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        score.setPosition(10, Values.WORLD_HEIGHT - 70, Align.topLeft);
        ui.addActor(score);


    }

    public void show() {
        init();
        super.show();
        timer.start();
    }

    public void hide() {
        super.hide();
        timer.stop();
    }

    protected void postAct() {
        if (player.getY() >= Values.WORLD_HEIGHT / 2) world.getCamera().position.y = player.getY();

        score.setText(String.format("%d", world.getScore()));

        if (world.getScore() > 2000 && bgState != 1) {
            bgState = 1;
            space.setPosition(0,1920);
            space.addAction(Actions.moveTo(0,0, 1f));
        }
        if (world.getScore() < 2001 && bgState != 1 && v < 0) {
            bgState = 1;
            space.setPosition(0,0 );
            space.addAction(Actions.moveTo(0, 1920, 1f));
        }
    }
    public void Plstore(){
        int plscore = 0;
        for(int i = 0; i < world.getScore(); i++){
            plscore = plscore + world.getScore();
        }
    }

}



