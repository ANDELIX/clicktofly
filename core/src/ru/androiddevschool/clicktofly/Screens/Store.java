package ru.androiddevschool.clicktofly.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import ru.androiddevschool.clicktofly.Controller.ScreenTraveler;
import ru.androiddevschool.clicktofly.Utils.Assets;
import ru.androiddevschool.clicktofly.Utils.Values;

import static com.badlogic.gdx.scenes.scene2d.ui.Table.Debug.actor;

/**
 * Created by 05k1102 on 15.04.2017.
 */
public class Store extends StdScreen {
    public Store(SpriteBatch batch) {
        super(batch);
        Button button;
        Label label;
        Button Skin00;
        button = new Button(Assets.get().buttonStyles.get("back"));
        button.setPosition(10,1805);
        button.addListener(new ScreenTraveler("Menu"));
        stage.addActor(button);

       Skin00 = new Button(Assets.get().buttonStyles.get("skin00"));

        button = new Button(Assets.get().buttonStyles.get("window"));
        button.setPosition(140, 1300);
       /* button.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y){
                stage.addActor(Skin00,215,1380);
            }
        });*/
        stage.addActor(button);



        button = new Button(Assets.get().buttonStyles.get("window"));
        button.setPosition(640, 1300);
        stage.addActor(button);
        button = new Button(Assets.get().buttonStyles.get("skin01"));
        button.setPosition(730, 1380);
        stage.addActor(button);


        button = new Button(Assets.get().buttonStyles.get("window"));
        button.setPosition(140, 900);
        stage.addActor(button);
        button = new Button(Assets.get().buttonStyles.get("skin02"));
        button.setPosition(215, 980);
        stage.addActor(button);
        button = new Button(Assets.get().buttonStyles.get("window"));
        button.setPosition(640, 900);
        stage.addActor(button);
        button = new Button(Assets.get().buttonStyles.get("skin03"));
        button.setPosition(770, 980);
        button.setSize(100,200);
        stage.addActor(button);

        button = new Button(Assets.get().buttonStyles.get("window"));
        button.setPosition(140, 500);
        stage.addActor(button);
        button = new Button(Assets.get().buttonStyles.get("window"));
        button.setPosition(640, 500);
        stage.addActor(button);

        label = new Label(  , new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(800 - label.getWidth() / 2, 1850);
        stage.addActor(label);
    }


}
