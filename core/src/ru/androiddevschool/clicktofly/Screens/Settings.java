package ru.androiddevschool.clicktofly.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import ru.androiddevschool.clicktofly.Controller.ScreenTraveler;
import ru.androiddevschool.clicktofly.Utils.Assets;

/**
 * Created by 05k1102 on 08.04.2017.
 */
public class Settings extends StdScreen {
    public Settings(SpriteBatch batch) {
        super(batch);
        Label label;
        Button button;
        button = new Button(Assets.get().buttonStyles.get("back"));
        button.setPosition(10,1805);
        button.addListener(new ScreenTraveler("Menu"));
        stage.addActor(button);

        button = new Button(Assets.get().buttonStyles.get("sound"));
        button.setPosition(540 - button.getWidth() / 2, 800);
        stage.addActor(button);

        label = new Label("Settings", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(540 - label.getWidth() / 2, 1400);
        stage.addActor(label);


    }

}
