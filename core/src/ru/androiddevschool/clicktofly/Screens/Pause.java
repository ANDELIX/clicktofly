package ru.androiddevschool.clicktofly.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

import ru.androiddevschool.clicktofly.Controller.ScreenTraveler;
import ru.androiddevschool.clicktofly.Utils.Assets;
import ru.androiddevschool.clicktofly.Utils.Values;

/**
 * Created by Andrey on 03.04.2017.
 */

public class Pause extends StdScreen {
    public Pause(SpriteBatch batch) {
        super(batch);
        Button button;
        Label label;
        Table table = new Table();
        table.setFillParent(true);
        Table t = new Table();
        table.setFillParent(true);

        button = new Button(Assets.get().buttonStyles.get("sound"));
        button.setPosition(Values.WORLD_WIDTH / 2, 850, Align.bottom);
        stage.addActor(button);

        label = new Label("Pause", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(540 - label.getWidth() / 2, 1400);
        stage.addActor(label);
        label = new Label("Restart", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(540 - label.getWidth() / 2, 1150);
        label.addListener(new ScreenTraveler("Play"));
        stage.addActor(label);
        label = new Label("Menu", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(540 - label.getWidth() / 2, 1050);
        label.addListener(new ScreenTraveler("Menu"));
        stage.addActor(label);

        button = new Button(Assets.get().buttonStyles.get("back"));
        button.setPosition(10,1805);
        button.addListener(new ScreenTraveler("Play"));
        stage.addActor(button);

        ui.addActor(table);
        ui.addActor(t);
    }
}