package ru.androiddevschool.clicktofly.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import ru.androiddevschool.clicktofly.Utils.Values;

/**
 * Created by 05k1102 on 01.04.2017.
 */
class StdScreen implements Screen {
    protected OrthographicCamera camera;
    protected Stage stage;
    protected OrthographicCamera uicamera;
    protected Stage ui;
    protected OrthographicCamera bgcamera;
    protected Stage bg;
    protected InputMultiplexer multiplexer;

    public StdScreen(SpriteBatch batch) {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(false);
        stage = new Stage(new StretchViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, camera), batch);

        uicamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        uicamera.setToOrtho(false);
        ui = new Stage(new StretchViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, uicamera), batch);

        bgcamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        bgcamera.setToOrtho(false);
        bg = new Stage(new StretchViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, bgcamera), batch);
    }

    protected void draw(){
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        bg.draw();
        stage.draw();
        ui.draw();
    }

    protected void act(float delta){
        bg.act(delta);
        ui.act(delta);
        stage.act(delta);
    }

    protected void postAct(){
    }

    @Override
    public void show() {
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(ui);
        multiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render(float delta) {
        act(delta);
        postAct();
        draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
