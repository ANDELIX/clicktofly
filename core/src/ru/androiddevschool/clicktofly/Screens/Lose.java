package ru.androiddevschool.clicktofly.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import ru.androiddevschool.clicktofly.Controller.ScreenTraveler;
import ru.androiddevschool.clicktofly.Utils.Assets;

/**
 * Created by Максим on 18.05.2017.
 */

public class Lose extends StdScreen {
    public Lose(SpriteBatch batch) {
        super(batch);
        Label label;
        label = new Label("!!!Game over!!!", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(540 - label.getWidth() / 2, 1200);
        stage.addActor(label);
        label = new Label("Restart", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(540 - label.getWidth() / 2, 900);
        label.addListener(new ScreenTraveler("Play"));
        stage.addActor(label);
        label = new Label("Menu", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(540 - label.getWidth() / 2, 600);
        label.addListener(new ScreenTraveler("Menu"));
        stage.addActor(label);

    }
}