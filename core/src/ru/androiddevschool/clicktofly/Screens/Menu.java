package ru.androiddevschool.clicktofly.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import ru.androiddevschool.clicktofly.Controller.ScreenTraveler;
import ru.androiddevschool.clicktofly.Utils.Assets;

/**
 * Created by 05k1102 on 01.04.2017.
 */
public class Menu extends StdScreen {
    public Menu(SpriteBatch batch) {
        super(batch);
        Label label;
        Table table = new Table();
        table.setFillParent(true);
        Button button;
        label = new Label("CLICK", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(540 - label.getWidth() / 2, 1400);
        stage.addActor(label);
        label = new Label("TO", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(540 - label.getWidth() / 2, 1300);
        stage.addActor(label);
        label = new Label("FLY", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(540 - label.getWidth() / 2, 1200);
        stage.addActor(label);


        label = new Label("PLAY", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(540 - label.getWidth() / 2, 750);
        stage.addActor(label);
        label.addListener(new ScreenTraveler("Play"));

        button = new Button(Assets.get().buttonStyles.get("settings"));
        button.setPosition(10, 10);
        button.addListener(new ScreenTraveler("Settings"));
        stage.addActor(button);

        button = new Button(Assets.get().buttonStyles.get("sound"));
        button.setPosition(870, 10);
        stage.addActor(button);

        button = new Button(Assets.get().buttonStyles.get("shop"));
        button.setPosition(280, 10);
        button.addListener(new ScreenTraveler("Store"));
        stage.addActor(button);


        label = new Label("?", new Label.LabelStyle(Assets.get().fonts.get("menuSymbol"), null));
        label.setPosition(650 - label.getWidth() / 2, 10);
        stage.addActor(label);
        label.addListener(new ScreenTraveler("Help"));
/*
        label = new Label("\uE800", new Label.LabelStyle(Assets.get().fonts.get("menuSymbol"), null));
        label.setPosition(870 - label.getWidth() / 2, 50);
        stage.addActor(label);
*/
        ui.addActor(table);
    }
}