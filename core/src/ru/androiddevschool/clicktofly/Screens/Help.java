package ru.androiddevschool.clicktofly.Screens;

        import com.badlogic.gdx.graphics.g2d.SpriteBatch;
        import com.badlogic.gdx.scenes.scene2d.ui.Button;
        import com.badlogic.gdx.scenes.scene2d.ui.Label;

        import ru.androiddevschool.clicktofly.Controller.ScreenTraveler;
        import ru.androiddevschool.clicktofly.Utils.Assets;

/**
 * Created by Максим on 23.04.2017.
 */

public class Help extends StdScreen {
    public Help(SpriteBatch batch) {
        super(batch);
        Label label;
        Button button;
        button = new Button(Assets.get().buttonStyles.get("back"));
        button.setPosition(10,1805);
        button.addListener(new ScreenTraveler("Menu"));
        stage.addActor(button);
        label = new Label("Tap to fly", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(540 - label.getWidth() / 2, 1200);
        stage.addActor(label);
        label = new Label("and buy skins ", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(540 - label.getWidth() / 2, 1100);
        stage.addActor(label);
        label = new Label("or upgrades ", new Label.LabelStyle(Assets.get().fonts.get("menu"), null));
        label.setPosition(540 - label.getWidth() / 2, 1000);
        stage.addActor(label);
    }


}
