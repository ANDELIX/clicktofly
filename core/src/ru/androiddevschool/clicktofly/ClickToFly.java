package ru.androiddevschool.clicktofly;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.actions.EventAction;


import java.util.HashMap;

import ru.androiddevschool.clicktofly.Screens.*;

public class ClickToFly extends Game {
	private ClickToFly(){}
	private static ClickToFly instance = new ClickToFly();
	public static ClickToFly getInstance(){return instance;}

	private SpriteBatch batch;
	private HashMap<String, Screen> screens;

	@Override
	public void create() {

		batch = new SpriteBatch();
		screens = new HashMap<String, Screen>();
		Gdx.gl.glClearColor(11f / 255, 16f / 255, 29f / 255, 1);
		screens.put("Menu", new Menu(batch));
		screens.put("Play", new Play(batch));
		screens.put("Pause",new Pause(batch));
		screens.put("Store",new Store(batch));
		screens.put("Help", new Help(batch));
		screens.put("Settings", new Settings(batch));
		screens.put("Lose", new Lose(batch));
		setScreen("Menu");
	}

	public void setScreen(String name) {
		if (screens.containsKey(name))
			setScreen(screens.get(name));
	}
}
